import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ArmorWidget();

  }

}


class ArmorWidget extends StatefulWidget {
    @override
    _ArmorWidgetState createState() => _ArmorWidgetState();
}

class _ArmorWidgetState extends State<ArmorWidget> {
  bool _isClassic = true;

  @override
  Widget build(BuildContext context) {

    Widget titleSection = Container(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(  left: 100, bottom: 12),
                  child:
                    Text(_isClassic ? 'This is the Chief.' : 'This is the Arbiter.',
                        style: (_isClassic ?
                                TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.amberAccent)
                                :
                                TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.lightBlue))
                  ),

                ),
                Text(_isClassic ? '"I need a weapon." - Master Chief': '"Were it so easy." - Thel \'Vadam',
                  style: (_isClassic ?
                          TextStyle( color: Colors.amberAccent, fontStyle: FontStyle.italic, fontWeight: FontWeight.w600, fontSize: 16)
                          :
                          TextStyle( color: Colors.lightBlue, fontStyle: FontStyle.italic, fontWeight: FontWeight.w600, fontSize: 16))),
              ], // Children
            ),
          ),
          IconButton(
            alignment: Alignment.centerRight,
            icon: (_isClassic ? Icon(Icons.account_circle_rounded) : Icon(Icons.account_circle_rounded)),
            color: (_isClassic ? Colors.amberAccent: Colors.lightBlue),
            onPressed: _toggleClassic,
          ),
        ],
      ),
    );

    Column _buildButtonColumn(Color color, IconData icon, String label){
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color,),
          Container(
            margin: const EdgeInsets.only(top: 2),
            child: Text(label, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: color), softWrap: true,),
          ),
        ],
      );
    }


    Widget textSection = Container(
        padding: const EdgeInsets.only(top: 12, left: 16, right: 16),
        child: Text(_isClassic ?
          'Master Chief Petty Officer John-117 is a highly decorated and successful military officer from the planet Eridanus III.'
              ' Demonstrating a strong desire for victory and physical prowess from a young age, Dr. Catherine Halsey determined he\'d be an ideal candidate for the secret military initiative'
              ' known as the Spartan-II program. Quickly taking to his new role, John stood out amongst his peers as one of the strongest leaders and best Spartans alongside Jerome-92, '
              ' Kurt-051, and Fred-104. After graduating from the program, John, or Sierra-117, went on to become one of the greatest Spartans in history saving humanity as a whole on many occasions.'
          :
          'Thel \'Vadam is a former Supreme Commander of the Covenant army. Hailing from Sanghelios, Thel \'Vadam is an exceptional and battle-hardened warrior. He was stripped of his title'
              ' and honor when the Master Chief destroyed Installation 04 of the Halo array. He was inaugurated as the next Arbiter, an instrument of the Covenant prophets, which lead '
              'to his discovery of the true purpose of the Halo array. Upon learning so, the Arbiter made it his personal mission to separate the Elites from the Covenant and stop the prophets'
              'from destroying the Universe.',
          softWrap: true,
          style: (_isClassic ? TextStyle(color: Colors.amberAccent, fontSize: 14,)
                             : TextStyle(color: Colors.lightBlue, fontSize: 14))
        )
    );
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          (_isClassic ? _buildButtonColumn(Colors.amber, Icons.star, 'Highly Decorated\n        Officer')
                      : _buildButtonColumn(Colors.lightBlueAccent, Icons.warning_amber_rounded, 'Discovered the\n Prophets lied.')),
          (_isClassic ? _buildButtonColumn(Colors.amber, Icons.battery_charging_full, '          "Wake me, \n   when you need me."')
                      : _buildButtonColumn(Colors.lightBlueAccent, Icons.assistant_photo, 'United the humans\n    and the Elites.')),
          (_isClassic ? _buildButtonColumn(Colors.amber, Icons.attach_money , '      "Tell that\n to the Covenant."')
                      : _buildButtonColumn(Colors.lightBlueAccent, Icons.attribution_outlined, '"He was indeed my enemy,\n          but, in time,\n     I named him ally\n       ...even friend."'))
        ],
      ),
    );


    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        backgroundColor: (_isClassic ? Color.fromARGB(100, 60, 90, 31) : Color.fromARGB(100, 80, 0, 160)),
        appBar: AppBar(
          title: (_isClassic ? Text('Master Chief', style: TextStyle(color: Colors.amberAccent),)
                             : Text('Thel \'Vadam', style: TextStyle(color: Colors.lightBlue),)),
          backgroundColor: (_isClassic ? Color.fromARGB(100, 60, 90, 31) : Color.fromARGB(100, 80, 0, 160)),
        ),
        body: ListView(
          children: [
            Image.asset(_isClassic ? 'images/chief.jpg' : 'images/arby.jpg', width: 600, height: 240, fit: BoxFit.cover,),
            titleSection,
            buttonSection,
            textSection
          ],
        ),
      ),
    );
  }
  void _toggleClassic() {
    setState(() {
      if(_isClassic){
        _isClassic = false;
      } else {
          _isClassic = true;
        }
    });
  }
}